#QT-Dictionary

![](http://www.qt-corporation.com/wp-content/uploads/2017/02/qt-icon.png)

####A Simple Dictionary with powerfull features built on JAVA.


![screenshot](https://i.imgur.com/MM9cuN2.png)

## Main Features

* Big database of English Words and meaningfull in Vietnam
* Live search
  - Realtime search on every character you type
* Pronounce function help you to pronounce correctly
* Online translate ( Using Google Script + Google API)
* Paragraph Transale Support
* Add a sound button to play the word in English
* Add add word, edit and remove word also
* Database in .TXT file so you can modify easily

## How To Use

To clone and run this application, you'll need [Git](https://git-scm.com) and [JAVA 8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) installed on your computer. From your command line:

```bash
# Clone this repository
$ git clone https://nguyenhopquang@bitbucket.org/nguyenhopquang/qt-dictionary.git

# Go into the repository
$ cd qt-dictionary

# Go into files folder
$ cd src/sample

# Complie the app
$ javac Main.java

# Run the app
$ java Main
```

## Authors

We are the authors ^^

- [Nguyễn Hợp Quang](https://anonymousvn.org/author/nguyenhopquang)
- [Vũ Công Thi](https://www.facebook.com/thi.vucong)


## Support
- [Donate US](https://www.paypal.me/nguyenhopquang)
## You may also like...

- [C++ Car Racing](https://github.com/nguyenhopquang/CarRacing) - A small Car Racing written in C++ using SFML

## License

MIT

---

> Blog [anonymousvn.org](https://anonymousvn.org/) :
> GitHub [@nguyenhopquang](https://github.com/nguyenhopquang)