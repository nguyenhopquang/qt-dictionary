package sample;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.*;
import javafx.scene.control.*;
import java.io.*;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import sun.audio.*;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.io.FileOutputStream;
import com.voicerss.tts.AudioCodec;
import com.voicerss.tts.AudioFormat;
import com.voicerss.tts.Languages;
import com.voicerss.tts.VoiceParameters;
import com.voicerss.tts.VoiceProvider;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class Controller{
    @FXML private TextField searchInput, addWordVietnam, addWordEnglish,editWordEnglish,englishNewEnglishWord,englishNewVietNamWord,removeEnglishWord;
    @FXML private Label inputedText, soundWord;
    @FXML private TextArea meaning,searchOnlineInput;
    @FXML private ImageView soundImage;
    @FXML private ListView<String> listSuggestedWord = new ListView<String>();
    private ArrayList<String> englishWord = new ArrayList<String>();
    private ArrayList<String> vietnamMeaning = new ArrayList<String>();
    private ArrayList<String> soundMeaning = new ArrayList<String>();

    /**
     * Hàm chạy ngay khi chương trình chạy
     * Hàm đọc dữ liệu từ điển từ file data.txt sau đó cho vào arrayList
     * Hàm sẽ lắng nghe các thay đổi ở ô tìm kiếm và action ở các button để gọi các hàm tương ứng
     */
    public void initialize(){
        DictionaryManagement importFile = new DictionaryManagement();
        try {
            String content= importFile.readFile("src/databases/data.txt", Charset.defaultCharset());
            String[] words = content.split("@");
            for (String word: words) {
                String result[] = word.split("\r?\n", 2);
                if (result.length>1) {
                    if (result[0].contains("/")) {
                        String firstmeaning = result[0].substring(0,result[0].indexOf("/"));
                        String lastSoundMeaning =result[0].substring(result[0].indexOf("/"), result[0].length());
                        englishWord.add(firstmeaning);
                        soundMeaning.add(lastSoundMeaning);
                    } else {
                        englishWord.add(result[0]);
                        soundMeaning.add("");
                    }
                    vietnamMeaning.add(result[1]);
                }
            }

            //Khởi tạo List từ tiếng anh để đưa vào listView
            ObservableList<String> suggestText = FXCollections.observableArrayList(englishWord);
            listSuggestedWord.setItems(suggestText);

            //Lắng nghe các thay đổi ở ô tìm kiếm để đưa ra gợi ý các từ bằng các gọi hàm getSuggestedWords
            searchInput.textProperty().addListener((observable, oldValue, newValue) -> {
               getSuggestedWords(newValue);
            });

            //Lắng nghe khi click vào các item trong listView để tìm từ được click và hiển thị nghĩa vào ô định nghĩa.
            listSuggestedWord.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
                public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                        boolean existed= false;
                        inputedText.setText(newValue);
                        for (int i=0;i<englishWord.size();i++) {
                            if (newValue.equals(englishWord.get(i))) {
                                existed= true;
                                meaning.setText(vietnamMeaning.get(i));
                                soundWord.setText(soundMeaning.get(i));
                                break;
                            }
                        }
                }
            });
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
    public void searchOnlineWord(ActionEvent event) throws Exception {
        String inputText = searchOnlineInput.getText();
        inputedText.setText(inputText);
        String googleResponse = googleTranslate("en", "vi", inputText);
        meaning.setText(googleResponse);
    }
    public static String googleTranslate(String langFrom, String langTo, String text) throws IOException {
        String urlStr = "https://script.google.com/macros/s/AKfycbygCmwOD1yD_xyXmiswF-ivFayva1Fo4bu17V6U_eFS2wbP4Edf/exec" +
                "?q=" + URLEncoder.encode(text, "UTF-8") +
                "&target=" + langTo +
                "&source=" + langFrom;
        URL url = new URL(urlStr);
        StringBuilder response = new StringBuilder();
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestProperty("User-Agent", "Mozilla/5.0");
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        return response.toString();
    }

    public void imageOnClick(MouseEvent events) throws Exception {

        //API gọi tới nhà cung cấp để lấy file phát âm theo từ khóa word
        VoiceProvider tts = new VoiceProvider("a3889c418b3d4b4fb32dec48e3d1cfec");
        String word = inputedText.getText();
        VoiceParameters params = new VoiceParameters(word, Languages.English_UnitedStates);
        params.setCodec(AudioCodec.WAV);
        params.setFormat(AudioFormat.Format_44KHZ.AF_44khz_16bit_stereo);
        params.setBase64(false);
        params.setSSML(false);
        params.setRate(0);
        byte[] voice = tts.speech(params);

        // Lưu file âm thanh tải về vào file voice.mp3
        FileOutputStream fos = new FileOutputStream("src/output_mp3/voice.mp3");
        fos.write(voice, 0, voice.length);
        fos.flush();
        fos.close();

        // Mở file mp3 bằng FileInputStream
        String gongFile = "src/output_mp3/voice.mp3";
        InputStream in = new FileInputStream(gongFile);

        // Tạo audiostream từ FileInputStream
        AudioStream audioStream = new AudioStream(in);

        // Mở file âm thanh vừa tải về
        AudioPlayer.player.start(audioStream);
    }

    /**
     * Hàm getSuggestedWords là hàm chỉnh sửa lại listView của danh sách các từ khóa được gợi ý
     * Hàm sẽ tìm các từ khóa bắt đầu bằng từ được nhập vào ô tìm kiếm
     * @param enteredKeyword là keyword được nhập vào
     */
    public void getSuggestedWords(String enteredKeyword) {
        ArrayList<String> suggestedWords = new ArrayList<String>();
        for(int i=0;i< englishWord.size();i++)
        {
            if(englishWord.get(i).startsWith(enteredKeyword.toLowerCase()))
            {
                suggestedWords.add(englishWord.get(i));
            }
        }
        ObservableList<String> suggestText = FXCollections.observableArrayList(suggestedWords);
        listSuggestedWord.setItems(suggestText);
    }

    /**
     * Hàm searchWord là hàm nhận từ tìm kiếm từ ô search và set từ này vào ô inpitedText để sử dụng về sau
     * @param event không dùng đến
     */
    public void searchWord(ActionEvent event) {
        String inputText = searchInput.getText();
        inputedText.setText(inputText);
    }
    public void addWord(ActionEvent event) {
        String english = addWordEnglish.getText();
        boolean existed= false;
        for (int i=0;i<englishWord.size();i++) {
            if (english.equals(englishWord.get(i))) {
                existed= true;
                break;
            }
        }
        if (!existed) {
            englishWord.add(english);
            String vietnam = addWordVietnam.getText();
            vietnamMeaning.add(vietnam);
            soundMeaning.add("");

            ObservableList<String> suggestText = FXCollections.observableArrayList(englishWord);
            listSuggestedWord.setItems(suggestText);

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Thông báo");
            alert.setHeaderText(null);
            alert.setContentText("Thêm từ thành công!");
            alert.showAndWait();
        } else {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Thông báo");
            alert.setHeaderText(null);
            alert.setContentText("Từ này đã tồn tại!");
            alert.showAndWait();
        }
    }
    public void editWord(ActionEvent event) {
        String editEN = editWordEnglish.getText();
        boolean existed= false;
        String newEnglishMeaning = englishNewEnglishWord.getText();
        String newVietnamMeaning = englishNewVietNamWord.getText();
        for (int i=0;i<englishWord.size();i++) {
            if (editEN.equals(englishWord.get(i))) {
                vietnamMeaning.set(i, newVietnamMeaning);
                soundMeaning.set(i,"");
                englishWord.set(i, newEnglishMeaning);
                existed= true;
                break;
            }
        }
        if (existed) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Thông báo");
            alert.setHeaderText(null);
            alert.setContentText("Sửa từ thành công!");
            alert.showAndWait();
        } else {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Thông báo");
            alert.setHeaderText(null);
            alert.setContentText("Không tìm thấy tù cần sửa!");
            alert.showAndWait();
        }
    }
    public void removeWord(ActionEvent event){
        String removeWord = removeEnglishWord.getText();
        boolean existed= false;
        for (int i=0;i<englishWord.size();i++) {
            if (removeWord.equals(englishWord.get(i))) {
                englishWord.remove(i);
                vietnamMeaning.remove(i);
                soundMeaning.remove(i);
                existed= true;
                break;
            }
        }
        if (existed) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Thông báo");
            alert.setHeaderText(null);
            alert.setContentText("Xóa từ thành công!");
            alert.showAndWait();
        } else {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Thông báo");
            alert.setHeaderText(null);
            alert.setContentText("Không tìm thấy tù cần xóa!");
            alert.showAndWait();
        }
    }

}
